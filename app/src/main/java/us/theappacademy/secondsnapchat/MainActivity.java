package us.theappacademy.secondsnapchat;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.backendless.Backendless;

public class MainActivity extends AppCompatActivity {


    public static final String APP_ID = "AC1EB6B6-1EFD-1336-FFC0-E277ACB5FC00";
    public static final String SECRET_KEY = "78422A59-7346-3544-FFB8-6AACE45A0900";
    public static final String VERSION = "v1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //MainMenuFragment mainMenu = new MainMenuFragment();
          LoginRegisterFragment loginRegister = new LoginRegisterFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.container, loginRegister).commit();
        Backendless.initApp(this, APP_ID, SECRET_KEY, VERSION);
        if (Backendless.UserService.loggedInUser() == ""){
            LoginRegisterFragment loginRegisterFragment = new LoginRegisterFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.container, loginRegisterFragment).commit();
        }else{
            
        }
    }
}
