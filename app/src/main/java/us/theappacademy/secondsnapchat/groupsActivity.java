package us.theappacademy.secondsnapchat;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class groupsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_groups);

        GroupsFragment mainMenu = new GroupsFragment();

        getSupportFragmentManager().beginTransaction().add(R.id.container, mainMenu).commit();
    }
}
