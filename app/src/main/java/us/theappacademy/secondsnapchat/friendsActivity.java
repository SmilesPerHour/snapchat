package us.theappacademy.secondsnapchat;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class friendsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friends);

        FriendsFragment mainMenu = new FriendsFragment();

        getSupportFragmentManager().beginTransaction().add(R.id.container, mainMenu).commit();
    }
}
