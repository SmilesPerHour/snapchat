package us.theappacademy.secondsnapchat;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class loginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        LoginFragment mainMenu = new LoginFragment();

        getSupportFragmentManager().beginTransaction().add(R.id.loginbutton, mainMenu).commit();

    }
}
