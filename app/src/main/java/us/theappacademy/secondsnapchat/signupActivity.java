package us.theappacademy.secondsnapchat;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class signupActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        signupFragment mainMenu = new signupFragment();

        getSupportFragmentManager().beginTransaction().add(R.id.registerContainer, mainMenu).commit();
    }
}
